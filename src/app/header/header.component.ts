import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  menuBtn:any;
  menu:any;


  ngOnInit(): void {
    this.menuBtn = document.getElementById('mainMenuBtn');
    this.menu = document.getElementById('menuHolder');
  }

  onMenuClicked() {
      this.menuBtn.classList.toggle('menuActive')
      this.menu.classList.toggle('showMenu');
  }

}
