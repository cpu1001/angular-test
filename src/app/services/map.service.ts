import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  constructor( private http: HttpClient) { }

  callMarkersAPI() : Observable <any> {
    let apiUrl = 'https://places.ls.hereapi.com/places/v1/browse?in=42.697858,23.32172;r=2000&cat=hotel&apiKey=z7TsGNGWW_seWxghJzVVQMXR9fC0JYa-PI2YIj_V6tw'
    return this.http.get<any>(apiUrl)
  }
}
