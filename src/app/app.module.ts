import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core'
import { HttpClientModule } from '@angular/common/http'
import { DragScrollModule } from 'ngx-drag-scroll';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    DragScrollModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAa0xsAYMF2kpvz0AvTj1GAbyWgt6ZQINE'
    })
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }