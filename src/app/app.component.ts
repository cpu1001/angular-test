import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MapService } from './services/map.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'full-stack-challenge';

  // MAP SETTINGS
  lat = 42.6984;
  lng = 23.3212;
  zoom = 16;
  bounds = null;
  markers = [];
  locations = [];

  constructor(private http: HttpClient, private mapService: MapService) {}

  ngOnInit() {
    this.mapService.callMarkersAPI().subscribe(data => {
      this.locations = data.results.items;
      this.setLocationMarkers();
    }, 
    error => {
      console.log('error >>', error)
    });
  }

  setLocationMarkers() {
    this.locations.forEach(location => {
      let tempMarker: any = {};
      tempMarker['lat'] = location.position[0];
      tempMarker['lng'] = location.position[1];
      tempMarker['label'] = location.title;
      tempMarker['active'] = false;
      tempMarker['iconUrl'] = "/assets/img/home-default-svg.png"
      tempMarker['iconUrlActive'] = "/assets/img/home-active-svg.png"
      this.markers.push(tempMarker);
    });
  }

  onMarkerClicked(marker, index) {
    this.setActiveItem(index);
    this.setActiveMarker(marker, index)
  }

  setActiveMarker(marker, index) {
    this.markers.forEach(item => {
      if(item.active) {
        item.active = false;
      }
    })

    this.markers[index].active = true;
  }

  setActiveItem(index) {
    let elmnt = document.getElementById(index);
    let previousElement = document.getElementsByClassName('activeItem');
    if (previousElement.length > 0) {
      previousElement[0].classList.remove('activeItem')
    }
    elmnt.classList.toggle('activeItem');
    elmnt.scrollIntoView({ behavior: 'smooth', inline: 'start' });
  }
}